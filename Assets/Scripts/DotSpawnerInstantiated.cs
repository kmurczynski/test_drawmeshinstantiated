using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotSpawnerInstantiated : MonoBehaviour
{
    private const int MaxMeshesInBatch = 1023;

    [SerializeField]
    private Mesh mesh;
    [SerializeField]
    private int quantity;
    [SerializeField]
    private float range;
    [SerializeField]
    private Material material;

    private Matrix4x4[][] matrices;
    private int lastElementCount;


    private void Start()
    {
        Setup();
    }

    private void Update()
    {
        for (int i = 0; i < matrices.Length - 1; i++)
        {
            Graphics.DrawMeshInstanced(mesh, 0, material, matrices[i], MaxMeshesInBatch);
        }

        Graphics.DrawMeshInstanced(mesh, 0, material, matrices[matrices.Length - 1], lastElementCount);
    }

    public void Setup()
    {
        int drawCount = Mathf.CeilToInt(quantity / MaxMeshesInBatch);
        lastElementCount = quantity % MaxMeshesInBatch;
        if (lastElementCount == 0)
        {
            lastElementCount = MaxMeshesInBatch;
        }
        matrices = new Matrix4x4[drawCount + 1][];

        Matrix4x4[] matric;
        for (int i = 0; i < drawCount; i++)
        {
            matric = new Matrix4x4[quantity];
            for (int j = 0; j < quantity; j++)
            {
                Vector3 position = new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));
                Quaternion rotation = Quaternion.identity;
                Vector3 scale = Vector3.one;

                Matrix4x4 mat = Matrix4x4.TRS(position, rotation, scale);

                matric[j] = mat;
                matrices[i] = matric;
            }
        }
        matric = new Matrix4x4[lastElementCount];
        for (int i = 0; i < lastElementCount; i++)
        {
            Vector3 position = new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));
            Quaternion rotation = Quaternion.identity;
            Vector3 scale = Vector3.one;

            Matrix4x4 mat = Matrix4x4.TRS(position, rotation, scale);

            matric[i] = mat;
        }

        matrices[matrices.Length - 1] = matric;
    }
}
